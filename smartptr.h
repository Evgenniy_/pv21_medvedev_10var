#ifndef SMARTPTR_H
#define SMARTPTR_H
#include <QPushButton>
template<class T>
class Smartptr
{
private:
    T* ptr;
public:
    Smartptr(T* newptr);
    ~Smartptr();

    T* operator->();
};

#endif // SMARTPTR_H
