#include "smartptr.h"

template<class T>
Smartptr<T>::Smartptr(T* newptr):ptr(newptr){}

template<class T>
Smartptr<T>::~Smartptr()
{
    //delete ptr;
}

template<class T>
T* Smartptr<T>::operator->()
{
    return ptr;
}
