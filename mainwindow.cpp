#include "mainwindow.h"
#include "smartptr.h"
#include "smartptr.cpp"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <memory>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow), memoryValue(0), memoryValueUse(false)
{
    ui->setupUi(this);


    connect(ui->pushButton_0, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_1, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_9, SIGNAL(clicked()), this, SLOT(numbers()));
    connect(ui->pushButton_plusMinus, SIGNAL(clicked()), this, SLOT(operations()));
    connect(ui->pushButton_sqrt, SIGNAL(clicked()), this, SLOT(operations()));
    connect(ui->pushButton_sqr, SIGNAL(clicked()), this, SLOT(operations()));
    connect(ui->pushButton_backspace, SIGNAL(clicked()), this, SLOT(operations()));
    connect(ui->pushButton_clean, SIGNAL(clicked()), this, SLOT(memory()));
    connect(ui->pushButton_MC, SIGNAL(clicked()), this, SLOT(mmemory()));
    connect(ui->pushButton_MR, SIGNAL(clicked()), this, SLOT(memory()));
    connect(ui->pushButton_MS, SIGNAL(clicked()), this, SLOT(memory()));
    connect(ui->pushButton_sum, SIGNAL(clicked()), this, SLOT(calcOperations()));
    connect(ui->pushButton_umn, SIGNAL(clicked()), this, SLOT(calcOperations()));
    connect(ui->pushButton_division, SIGNAL(clicked()), this, SLOT(calcOperations()));
    connect(ui->pushButton_subtract, SIGNAL(clicked()), this, SLOT(calcOperations()));

    ui->pushButton_sum->setCheckable(true);
    ui->pushButton_umn->setCheckable(true);
    ui->pushButton_division->setCheckable(true);
    ui->pushButton_subtract->setCheckable(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::numbers()
{

    Smartptr<QPushButton> button ((QPushButton*)sender()); //smartptr

    if(ui->currentValue->text().contains('.') && button->text()== "0")
    {
        ui->currentValue->setText(ui->currentValue->text() + button->text());
    }
    else
    {
        double num;
        num = (ui->currentValue->text() + button->text()).toDouble();

        ui->currentValue->setText(QString::number(num, 'g', 8));
    }

}

void MainWindow::memory()
{
    Smartptr<QPushButton> button ((QPushButton*)sender()); //smartptr
    double num = (ui->currentValue->text()).toDouble();

    if(button->text() == "C")
        {
            ui->currentValue->setText("0");
            ui->oldValue->setText("");
            ui->pushButton_sum->setChecked(false);
            ui->pushButton_umn->setChecked(false);
            ui->pushButton_division->setChecked(false);
            ui->pushButton_subtract->setChecked(false);
     }else if(button->text() == "MC")
     {
        memoryValueUse = false;
     }else if(button->text() == "MS")
     {
        memoryValue= num;
        memoryValueUse = true;
        ui->currentValue->setText("0");
     }else if(button->text() == "MR")
     {
        if(memoryValueUse)
        {
            num = memoryValue;
            ui->currentValue->setText(QString::number(num, 'g', 8));
        }
    }
}

void MainWindow::operations()
{
    Smartptr<QPushButton> button ((QPushButton*)sender()); //smartptr
    double num = (ui->currentValue->text()).toDouble();

    if(button->text()=="+/-")
    {
         ui->currentValue->setText(QString::number(-1.0*num, 'g', 8));
    }else if(button->text()=="sqrt")
    {
        if(num<0){
            QMessageBox::warning(this, "Ошибка вычислений", "Корень из отрицательного числа");
        }
        else
            ui->currentValue->setText(QString::number(sqrt(num), 'g', 8));
    }else if(button->text()=="x^2")
    {
        ui->currentValue->setText(QString::number(num*num, 'g', 8));
    }else if(button->text()=="<-")
    {
        QString str = ui->currentValue->text();
        str.resize(str.size()-1);
        if(str.size()==0)
            str="0";
        ui->currentValue->setText(str);
    }
}


void MainWindow::calcOperations()
{
    Smartptr<QPushButton> button ((QPushButton*)sender()); //smartptr
    ui->oldValue->setText(ui->currentValue->text()+button->text());
    ui->currentValue->setText("0");
    button->setChecked(true);
}


void MainWindow::on_pushButton_dot_clicked()
{
    if(!(ui->currentValue->text().contains('.')))
         ui->currentValue->setText(ui->currentValue->text()+'.');
}

void MainWindow::on_pushButton_result_clicked()
{
    QString first = ui->oldValue->text();
    first.resize(first.size()-1);
    QString second = ui->currentValue->text();

    if(ui->pushButton_sum->isChecked())
    {
        ui->oldValue->setText("");
        ui->currentValue->setText(QString::number(first.toDouble() + second.toDouble(), 'g', 8));
        ui->pushButton_sum->setChecked(false);

    }else if(ui->pushButton_umn->isChecked())
    {
        ui->oldValue->setText("");
        ui->currentValue->setText(QString::number(first.toDouble() * second.toDouble(), 'g', 8));
        ui->pushButton_umn->setChecked(false);
    }else if(ui->pushButton_division->isChecked())
    {
        if(second.toDouble() == 0)
        {
            QMessageBox::warning(this, "Результат операции", "Нельзя делить на ноль");
            ui->oldValue->setText("");
            ui->currentValue->setText("0");
            ui->pushButton_division->setChecked(false);
        }
        else
        {
            ui->oldValue->setText("");
            ui->currentValue->setText(QString::number(first.toDouble() / second.toDouble(), 'g', 8));
            ui->pushButton_division->setChecked(false);
        }
    }else if(ui->pushButton_subtract->isChecked())
    {
        ui->oldValue->setText("");
        ui->currentValue->setText(QString::number(first.toDouble() - second.toDouble(), 'g', 8));
        ui->pushButton_subtract->setChecked(false);
    }
}

