#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    double memoryValue;
    bool memoryValueUse;
private slots:
    void numbers();
    void operations();
    void calcOperations();
    void on_pushButton_result_clicked();
    void on_pushButton_dot_clicked();
    void memory();
};
#endif // MAINWINDOW_H
